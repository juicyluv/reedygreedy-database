create schema if not exists main authorization postgres;
grant all on schema main to postgres;
grant usage on schema main to web;